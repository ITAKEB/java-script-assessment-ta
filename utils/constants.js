const ERROR_EMPTY_USERNAME = "Epic sadface: Username is required";

const ERROR_EMPTY_PASSWORD = "Epic sadface: Password is required";

const HEADER_TITLE = "Swag Labs";

module.exports = {
  ERROR_EMPTY_USERNAME,
  ERROR_EMPTY_PASSWORD,
  HEADER_TITLE,
};
