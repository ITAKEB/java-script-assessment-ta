const BasePage = require("./base.page");

class LoginPage extends BasePage {
  async open() {
    await browser.url("https://www.saucedemo.com/");
  }

  get usernameInput() {
    //This locator is by xpath
    return $(`//input[@id="user-name"]`);
  }

  get passwordInput() {
    return $("#password");
  }

  get loginButton() {
    return $("#login-button");
  }

  get errorContainer() {
    return $(".error-message-container");
  }

  async typeCredentialsAs(user, password) {
    await this.usernameInput.setValue(user);

    await this.passwordInput.setValue(password);
  }

  async clearCredentialsTyped() {
    await this.clearInput(this.usernameInput);

    await this.clearInput(this.passwordInput);
  }

  async loginAs(user, password) {
    await this.usernameInput.setValue(user);

    await this.passwordInput.setValue(password);

    await this.loginButton.click();
  }

  async loginAsStandardUser() {
    await this.loginAs("standard_user", "secret_sauce");
  }
}

module.exports = LoginPage;
