const BasePage = require("./base.page");
const HeaderComponent = require("./components/common/header.component");

class InventoryItemPage extends BasePage {
  constructor() {
    super();
    this.header = new HeaderComponent();
  }

  get addToCart() {
    return $(`.btn_primary.btn_inventory`);
  }
}

module.exports = InventoryItemPage;
