const BasePage = require("./base.page");
const HeaderComponent = require("./components/common/header.component");
const ItemComponent = require("./components/common/item.component");

class HomePage extends BasePage {
  constructor() {
    super();
    this.header = new HeaderComponent();
  }

  get inventoryItemsList() {
    return $(`.inventory_list`);
  }

  get allCartBtnsItemsList() {
    return this.inventoryItemsList.$$(`[id*="add-to-cart"]`);
  }

  getItemById(id) {
    return new ItemComponent(id);
  }
}

module.exports = HomePage;
