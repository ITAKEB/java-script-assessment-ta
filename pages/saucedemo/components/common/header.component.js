const BasePage = require("../../base.page");

class HeaderComponent extends BasePage {
  get cartIconButton() {
    return $("#shopping_cart_container");
  }

  get root() {
    return $("#header_container");
  }

  get appLogo() {
    return this.root.$(".primary_header .app_logo");
  }

  get cartIconBadge() {
    return this.root.$("#shopping_cart_container .shopping_cart_badge");
  }

  get burgerMenu() {
    return this.root.$("#react-burger-menu-btn");
  }

  get resetSideBarBtn() {
    return this.root.$("#reset_sidebar_link");
  }

  get logoutSideBarBtn() {
    return this.root.$("#logout_sidebar_link");
  }

  async resetAppState() {
    await this.customClick(this.burgerMenu);
    await this.customClick(this.resetSideBarBtn);
    await this.customClick(this.logoutSideBarBtn);
  }
}

module.exports = HeaderComponent;
