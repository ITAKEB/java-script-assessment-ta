class ItemComponent {
  constructor(id) {
    this.id = id;
  }

  get root() {
    return this.titleLink.$(`//ancestor::div[@class="inventory_item"]`);
  }

  get addToCartBtn() {
    return this.root.$(`//button[contains(@id, 'add-to-cart')]`);
  }

  get removeFromCartBtn() {
    return this.root.$(`//button[contains(@id, 'remove')]`);
  }

  get titleLink() {
    return $(`//a[@id='item_${this.id}_title_link']`);
  }
}

module.exports = ItemComponent;
