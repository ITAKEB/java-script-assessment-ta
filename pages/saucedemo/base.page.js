class BasePage {
  async customClick(element) {
    await element.waitForDisplayed({ timeout: 10000 });
    await element.waitForClickable({ timeout: 2000 });
    await element.click();
  }

  async clearInput(element) {
    await element.waitForClickable({ timeout: 2000 });
    await element.click();
    await browser.keys(["Control", "a"]);
    await browser.keys(["Delete"]);
  }
}

module.exports = BasePage;
