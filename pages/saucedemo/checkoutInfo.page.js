const BasePage = require("./base.page");
const HeaderComponent = require("./components/common/header.component");

class CheckoutInfoPage extends BasePage {
  constructor() {
    super();
    this.header = new HeaderComponent();
  }
  get firstNameInput() {
    return $("#first-name");
  }

  get lastNameInput() {
    return $("#last-name");
  }

  get postalCodeInput() {
    return $("#postal-code");
  }

  get continueBtn() {
    return $("#continue");
  }

  get errorContainer() {
    return $(".error-message-container.error");
  }

  get errorMessage() {
    return $("[data-test='error']");
  }
}

module.exports = CheckoutInfoPage;
