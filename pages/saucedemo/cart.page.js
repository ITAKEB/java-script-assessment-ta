const BasePage = require("./base.page");
const HeaderComponent = require("./components/common/header.component");

class CartPage extends BasePage {
  constructor() {
    super();
    this.header = new HeaderComponent();
  }

  get checkoutBtn() {
    return $("#checkout");
  }
}

module.exports = CartPage;
