const LoginPage = require("../../pages/saucedemo/login.page");
const { ERROR_EMPTY_USERNAME } = require("../../utils/constants");

const loginPage = new LoginPage();

describe("UC - 1 @UC-1", () => {
  beforeEach(async () => {
    await loginPage.open();
  });

  it("Test Login form with empty credentials ", async () => {
    await loginPage.typeCredentialsAs("any", "credentials");
    await loginPage.clearCredentialsTyped();
    const loginBtn = loginPage.loginButton;
    await loginPage.customClick(loginBtn);

    const errorContainer = await loginPage.errorContainer;
    await expect(errorContainer).toBeDisplayed();
    await expect(errorContainer).toHaveText(ERROR_EMPTY_USERNAME);
  });
});
