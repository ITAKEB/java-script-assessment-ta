const HomePage = require("../../pages/saucedemo/home.page");
const LoginPage = require("../../pages/saucedemo/login.page");
const { HEADER_TITLE } = require("../../utils/constants");

const loginPage = new LoginPage();
const homePage = new HomePage();

describe("UC - 3 @UC-3", () => {
  beforeEach(async () => {
    await loginPage.open();
  });

  it("Test Login form with credentials by passing Username & Password", async () => {
    await loginPage.loginAsStandardUser();
    const header = homePage.header;
    await expect(header.appLogo).toBeDisplayed();
    await expect(header.appLogo).toHaveText(HEADER_TITLE);
  });
});
