const LoginPage = require("../../pages/saucedemo/login.page");
const { ERROR_EMPTY_PASSWORD } = require("../../utils/constants");

const loginPage = new LoginPage();

describe("UC - 2 @UC-2", () => {
  beforeEach(async () => {
    await loginPage.open();
  });

  it("Test Login form with credentials by passing Username", async () => {
    await loginPage.typeCredentialsAs("any credentials", "password");
    const passwordInput = loginPage.passwordInput;
    await loginPage.clearInput(passwordInput);
    const loginButton = loginPage.loginButton;
    await loginPage.customClick(loginButton);

    const errorContainer = await loginPage.errorContainer;
    await expect(errorContainer).toBeDisplayed();
    await expect(errorContainer).toHaveText(ERROR_EMPTY_PASSWORD);
  });
});
